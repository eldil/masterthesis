#ifndef MASTERTHESIS_RANK_DATA_H
#define MASTERTHESIS_RANK_DATA_H

#include "file_data.h"
#include <utility>

struct RankData {
  
  RankData(int rank, ll score, std::pair<FileData,
      FileData> files, bool uncommon) :
    rank(rank), score(score), files(files), uncommon_fit(uncommon) {}
  
  int rank;
  ll score;
  std::pair<FileData, FileData> files;
  bool uncommon_fit;
};

#endif

