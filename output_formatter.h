#ifndef MASTERTHESIS_OUTPUT_FORMATTER_H
#define MASTERTHESIS_OUTPUT_FORMATTER_H

#include "output_character_data.h"
#include "preprocessed_file.h"
#include <string>
#include <curses.h>

class OutputFormatter {
 
 public:

  OutputFormatter();

  ~OutputFormatter();
  
  void display_files(const PreprocessedFile& first,
      const PreprocessedFile& second, const std::vector<int>& first_details,
      const std::vector<int>& second_details);

 private:

  void change_row(int start[], int end[], int column, int value);

  int get_file_data(const std::string& file, const std::vector<int>& details,
      std::vector<OutputCharacterData>& data);

  void display_file(const std::vector<OutputCharacterData>& data, WINDOW* pad,
      bool whitespace_visible = false);

  int get_color(int score);

  std::vector<OutputCharacterData> first_characters;
  std::vector<OutputCharacterData> second_characters;

};

#endif

