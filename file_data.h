#ifndef MASTERTHESIS_FILE_DATA_H
#define MASTERTHESIS_FILE_DATA_H

#include <string>
#include <boost/filesystem.hpp>

struct FileData {
  boost::filesystem::path file_path;
  std::string language;
  std::string id;
  std::string user;
  std::string contest;
  std::string problem;

  static bool sort_by_kind(const FileData& first, const FileData& second);

  std::string description(bool add_user = true) const;
};

bool operator< (const FileData& first, const FileData& second);

#endif

