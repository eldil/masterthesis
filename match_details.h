#ifndef MASTERTHESIS_MATCH_DETAILS_H
#define MASTERTHESIS_MATCH_DETAILS_H

#include <utility>

struct MatchDetails {

  typedef std::pair<std::pair<int, int>, int> Match;

  MatchDetails(int iscore, int score, Match match, bool uncommon) :
    iscore(iscore), score(score), start1(match.first.first),
    start2(match.first.second), length(match.second), uncommon_fit(uncommon) {}

  int iscore; //score of the substring before match extending
  int score; //score of the substring after match extending
  int start1; //starting point in the first file
  int start2; //starting point in the second file
  int length; //substring length

  bool uncommon_fit;
};

#endif

