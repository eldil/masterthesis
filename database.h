#ifndef MASTERTHESIS_DATABASE_H
#define MASTERTHESIS_DATABASE_H

#include "constants.h"
#include "preprocessed_file.h"
#include "file_reader.h"
#include "file_preprocessor.h"
#include "match_details.h"
#include "rank_data.h"
#include <string>
#include <vector>
#include <map>
#include <sqlite3.h>

class Database {

  typedef std::pair<int, int> FilesNumbersPair;
  typedef std::pair<std::pair<int, int>, std::pair<int, int> > Fit;

 public:

  Database();

  ~Database();

  bool initialize(std::string name);

  void add_train_data(const std::vector<FileData>& data);

  void add_test_data(const std::vector<FileData>& data);

  bool add_file(const PreprocessedFile& file);

  void rank_data(std::vector<RankData>& rank);

  void get_details(int rank_nr, PreprocessedFile& first_file,
      PreprocessedFile& second_file, std::vector<int>& first_details,
      std::vector<int>& second_details);

 private:

  void add_train_data(const PreprocessedFile& file);

  void add_test_data(const PreprocessedFile& file);

  void grade_data();

  //returns the match extent in the first and the second file <start, end)
  //modyfies the score for the match - the result parameter
  MatchDetails::Match extend_match(const std::vector<Token>& first_tokens,
      int first_start, const std::vector<Token>& second_tokens,
      int second_start, int token_size, int* result);
  
  void add_details(std::vector<int>& details, int score, int start, int end,
      const PreprocessedFile& file);

  void add_literal_fits(std::vector<Fit>& fits, int start1, int start2,
      int len, const PreprocessedFile& file1, const PreprocessedFile& file2);

  bool is_uncommon_fit(const std::string& inter1, const std::string& inter2);
  
  int get_score(int freq);

  //nr of occurences of each string in train data
  std::map<std::string, int> frequency; 

  std::vector<PreprocessedFile> files;

  //for each substring list of it's occurences in the test data
  //occurence = pair(file_nr, start location)
  std::map<std::string, std::vector<std::pair<int, int> > > mutual;

  //details of all the matches for each file pair
  std::map<FilesNumbersPair, std::vector<MatchDetails> > matches;
  
  //ranking of test data
  std::vector<std::pair<ll, FilesNumbersPair> > rank;

  sqlite3 *db;
  
  char* error_message;

  FileReader reader;

  FilePreprocessor preprocessor;
};

#endif

