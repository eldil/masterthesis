// vim:ts=4:sts=4:sw=4:expandtab
// TODO: ca-certificates location
// TODO: certificate path
#include "connection.h"

#include <iostream>
#include <fstream>
#include <map>
#include <sstream>

#include <boost/algorithm/string.hpp>

#include <curl/curl.h>

#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/transport/TSocket.h>
#include <thrift/transport/TSSLSocket.h>
#include <thrift/transport/TTransportUtils.h>

#include "satori-api/User.h"
#include "satori-api/Contestant.h"
#include "satori-api/Machine.h"

namespace satori {
    Connection::Configuration::Configuration() :
        thrift_port(0),
        blob_port(0),
        ssl(false) {
    }
    std::map<std::string, std::map<std::string, std::string>> config_parse(const std::string config_path) {
        std::map<std::string, std::map<std::string, std::string>> result;
        std::ifstream config(config_path.c_str());
        if (not config.good())
            return result;
        std::string section;
        while (not config.eof() ) {
            std::string line;
            std::getline(config, line);
            boost::algorithm::trim(line);
            if (line[0] == '#')
                continue;
            else if (line[0] == '[') {
                if (line[line.size()-1] == ']')
                    line.resize(line.size()-1);
                line = line.substr(1);
                boost::algorithm::trim(line);
                if (line.size() > 1) {
                    section = line;
                    result[section].size();
                }
            } else {
                std::vector<std::string> divided;
                boost::algorithm::split(divided, line, boost::algorithm::is_any_of("="), boost::algorithm::token_compress_on);
                if (divided.size() > 1) {
                    auto key = divided[0];
                    boost::algorithm::trim(key);
                    if (key != "") {
                        auto value = divided[1];
                        for (size_t i=2; i<divided.size(); i++) {
                            value += "=";
                            value += divided[i];
                        }
                        boost::algorithm::trim(value);
                        result[section][key] = value;
                    }
                }
            }
        }
        return result;
    }

    void host_parse(std::string& host, unsigned short int& thrift_port, unsigned short int& blob_port) { 
        std::vector<std::string> divided;
        boost::algorithm::split(divided, host, boost::algorithm::is_any_of(":"), boost::algorithm::token_compress_on);
        boost::algorithm::trim(divided[0]);
        host = divided[0];
        if (divided.size() > 1 and thrift_port == 0)
            thrift_port = atoi(divided[1].c_str());
        if (divided.size() > 2 and blob_port == 0)
            blob_port = atoi(divided[2].c_str());
    }
 
    void Connection::Configuration::setup() {
        if (config != "") {
            //TODO: check that file exists;
        } else {
            config = std::string(::getenv("HOME")) + "/.satori.cfg";
        }
        auto conf_map = config_parse(config);
        if (section == "") {
            if (conf_map.find("defaults") != conf_map.end())
                section = conf_map["defaults"]["section"];
        }
        host_parse(host, thrift_port, blob_port);
        if (section != "") {
            //TODO: check that section exists;
            auto& sec_map = conf_map[section];
            if (host == "" and sec_map.find("host") != sec_map.end())
                host = sec_map["host"];
            if (thrift_port == 0 and sec_map.find("thrift_port") != sec_map.end())
                thrift_port = atoi(sec_map["thrift_port"].c_str());
            if (blob_port == 0 and sec_map.find("blob_port") != sec_map.end())
                blob_port = atoi(sec_map["blob_port"].c_str());
            if (username == "" and sec_map.find("username") != sec_map.end())
                username = sec_map["username"];
            if (contestant == "" and sec_map.find("contestant") != sec_map.end())
                contestant = sec_map["contestant"];
            if (machine == "" and sec_map.find("machine") != sec_map.end())
                machine = sec_map["machine"];
            if (password == "" and sec_map.find("password") != sec_map.end())
                password = sec_map["password"];
            if (sec_map.find("ssl") != sec_map.end()) {
                std::string enable = sec_map["ssl"];
                boost::algorithm::to_lower(enable);
                boost::algorithm::trim(enable);
                if (enable == "1" or enable == "true" or enable == "yes")
                    ssl = true;
            }
            if (certificate == "" and sec_map.find("certificate") != sec_map.end())
                certificate = sec_map["certificate"];
        }
        host_parse(host, thrift_port, blob_port);
        //TODO: Check hostname
        //TODO: Check thrift port
        //TODO: Check blob port
        //TODO: Check that certificate file exists
        //TODO: Check that username/contestant/machine is set
        
    }

    Connection::Connection(const Connection::Configuration& config) :
        config(config) {
        this->config.setup();
    }

    void Connection::connect_protocol() {
        boost::shared_ptr<apache::thrift::transport::TSocket> socket;
        std::cerr << "Connecting to " << config.host << ":" << config.thrift_port << (config.ssl?" via SSL":"") << std::endl; 
        if (config.ssl) {
            if (not ssl_factory) {
                ssl_factory = boost::shared_ptr<apache::thrift::transport::TSSLSocketFactory>(new apache::thrift::transport::TSSLSocketFactory());
//                ssl_factory->ciphers("ALL:!ADH:!LOW:!EXP:!MD5:@STRENGTH");
                ssl_factory->loadTrustedCertificates("/etc/ssl/certs/ca-certificates.crt");
                if (config.certificate != "") {
                    std::cerr << "Loading certificate " << config.certificate << std::endl;
                    ssl_factory->loadTrustedCertificates(config.certificate.c_str());
                }
                ssl_factory->authenticate(true);
            }
            socket = ssl_factory->createSocket(config.host, config.thrift_port);
        } else {
            socket = boost::shared_ptr<apache::thrift::transport::TSocket>(new apache::thrift::transport::TSocket(config.host, config.thrift_port));
        }
        boost::shared_ptr<apache::thrift::transport::TTransport> transport(new apache::thrift::transport::TFramedTransport(socket));
        transport->open();
        protocol = boost::shared_ptr<apache::thrift::protocol::TProtocol>(new apache::thrift::protocol::TBinaryProtocol(transport));
        if (config.username != "") {
            std::cerr << "Authenticating user " << config.username << std::endl;
            UserClient authenticator(protocol);
            authenticator.User_authenticate(token, "", config.username, config.password);
        } else if (config.contestant != "") {
            std::cerr << "Authenticating contestant " << config.contestant << std::endl;
            ContestantClient authenticator(protocol);
            authenticator.Contestant_authenticate(token, "", config.contestant, config.password);
        } else if (config.machine != "") {
            std::cerr << "Authenticating machine " << config.machine << std::endl;
            MachineClient authenticator(protocol);
            authenticator.Machine_authenticate(token, "", config.machine, config.password);
        }
    }

    boost::shared_ptr<apache::thrift::protocol::TProtocol> Connection::connect_thrift() {
        if (not protocol or not protocol->getTransport()->isOpen())
            connect_protocol();
        return protocol;
    }
    const std::string& Connection::get_token() {
        connect_thrift();
        return token;
    }
    size_t write_data(void* ptr, size_t size, size_t nmemb, FILE* stream) {
        size_t written = fwrite(ptr, size, nmemb, stream);
        return written;
    }
    bool Connection::download_blob(const std::string& path, const std::string& model, size_t id, const std::string& group, const std::string& name) {
        std::string token = get_token();
        std::stringstream url_stream;
        url_stream << (config.ssl?"https://":"http://");
        url_stream << config.host << ":" << config.blob_port << "/blob/" << model << "/" << id << "/" << group << "/" << name;
        std::string url(url_stream.str());
        //std::cerr << "Downloading " << url << std::endl;
        //sator_token = token
        CURL* curl = curl_easy_init();
        if (curl) {
            FILE* fp = fopen(path.c_str(), "wb");
            //curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
            //curl_easy_setopt(curl, CURLOPT_CERTINFO, 1);
            curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
            curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
            curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
            if (config.ssl) {
//                curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER , 1);
//                curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST , 2);
                curl_easy_setopt(curl, CURLOPT_CAINFO , "/etc/ssl/certs/ca-certificates.crt");
                curl_easy_setopt(curl, CURLOPT_CAPATH , NULL);
                if (config.certificate != "")
                    curl_easy_setopt(curl, CURLOPT_CAINFO , config.certificate.c_str());
            }
            curl_slist *headerlist=NULL;
            char cookie_buf[32+token.size()];
            snprintf(cookie_buf, sizeof(cookie_buf), "Cookie: satori_token=%s", token.c_str());
            headerlist = curl_slist_append(headerlist, cookie_buf);
            curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
            auto res = curl_easy_perform(curl);
            curl_easy_cleanup(curl);
            curl_slist_free_all (headerlist);
            fclose(fp);
            if(res != 0)
                std::cerr << "CURL error: " << res << std::endl;
            return res == 0;
        }
        return false;
    }
}
