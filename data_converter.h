#ifndef MASTERTHESIS_DATA_CONVERTER_H
#define MASTERTHESIS_DATA_CONVERTER_H

#include "constants.h"
#include <vector>
#include <string>

class DataConverter {
 
 public:
  
  static void encode(const std::vector<uint>& from, std::string& to);
  
  static void decode(const std::string& from, std::vector<uint>& to);

  static uint hash(const std::string& s);

  static std::string md5(const std::string& s);

  static std::string time_to_string(int time);

private:

  static const uint PRIME = 2038074743;
  static std::hash<std::string> hash_function;
};

#endif

