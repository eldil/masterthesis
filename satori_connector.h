#ifndef MASTERTHESIS_SATORI_CONNECTOR_H
#define MASTERTHESIS_SATORI_CONNECTOR_H

#include "file_reader.h"
#include "file_data.h"

#include "satori-api/Contest.h"
#include "satori-api/Contestant.h"
#include "satori-api/ProblemMapping.h"
#include "satori-api/Submit.h"
#include "connection.h"

//#include <boost/shared_ptr.hpp>
#include <vector>
#include <map>

class SatoriConnector {

public:

  SatoriConnector();

  bool initialize();

  void get_files(std::vector<FileData>& output);

  void sort_files();

  bool add_files(
   const std::vector<std::pair<std::string, std::string>>& problems, 
   const std::string user = "");

private:

  bool initialized;
  std::vector<FileData> files;
  FileReader reader;

  std::unique_ptr<satori::Connection> connection;
  std::unique_ptr<ContestClient> contest_client;
  std::unique_ptr<ContestantClient> contestant_client;
  std::unique_ptr<SubmitClient> submit_client;
  std::unique_ptr<ProblemMappingClient> problem_client;
  std::string token;

  void add_files_for_problem(
    const std::string contest_name,
    const std::string problem_name,
    std::vector<FileData>& files);

};

#endif

