#include "data_converter.h"
#include "constants.h"
#include "data_converter.h"
#include "file_preprocessor.h"
#include <set>
#include <vector>
#include <boost/tokenizer.hpp>
#include <clang-c/Index.h>
using namespace std;

const map<string, string> FilePreprocessor::mapping = {{"and", "&&"}, /*{"auto", ??},*/
  {"and_eq", "&="}, {"bitand", "&"}, {"bitor", "|"},  {"char", "int"},
  {"char16_t", "int"}, {"char32_t", "int"}, {"const", ""}, {"double", "float"},
  {"explicit", ""}, {"extern", ""},  {"friend", ""}, {"inline", ""},
  {"long", "int"}, {"not", "!"}, {"not_eq", "!="}, {"nullptr", "NULL"},
  {"or", "||"}, {"or_eq", "|="}, {"private", ""}, {"protected", ""},
  {"public", ""}, {"short", "int"}, {"signed", "int"}, {"static", ""},
  {"struct", "class"}, {"unsigned", "int"}, {"virtual", ""}, {"volatile", ""},
  {"wchar_t", "int"}, {"xor", "^"}, {"xor_eq", "^="}
};

void print_extent(CXSourceRange range, CXTranslationUnit tunit) {
  CXToken* tokens = 0;
  unsigned int num_tokens = 0;
  clang_tokenize(tunit, range, &tokens, &num_tokens);
  for(int i = 0; i < num_tokens; ++i) {
    CXString cxtoken = clang_getTokenSpelling(tunit, tokens[i]);
    cout << clang_getCString(cxtoken) << " ";
    clang_disposeString(cxtoken);
  }
  cout << endl;
  clang_disposeTokens(tunit, tokens, num_tokens);
}

enum CXChildVisitResult visit(CXCursor cursor, CXCursor parent,
    CXClientData client_data) {
  print_extent(clang_getCursorExtent(cursor), (CXTranslationUnit) &client_data);
  return CXChildVisit_Recurse;
}

bool FilePreprocessor::process(PreprocessedFile& file) {

  CXIndex index = clang_createIndex(1, 0);

  vector<const char*> argv = {"-c", "-w", "-x", file.get_language().c_str()};
  if (FLAGS_cpp0x)
      argv.push_back("-std=c++0x");
  boost::escaped_list_separator<char> els("", " ", "\"\'");
  vector<string> aux;
  boost::tokenizer<boost::escaped_list_separator<char>> tok(FLAGS_cxxflags, els);
  for (auto & flag : tok) {
    aux.emplace_back(flag);
    argv.push_back(aux.back().c_str());
  }

  //Arguments: CXIndex cxindex, const char* const * command_line_args,
  //int num_command_line_args, struct CXUnsavedFile* unsaved_files,
  //unsigned num_unsaved_files, unsigned options){
  const char* file_path = file.get_path().c_str();
  CXTranslationUnit tunit = clang_parseTranslationUnit(index, file_path, argv.data(),
      argv.size(), NULL, 0, CXTranslationUnit_DetailedPreprocessingRecord);

  if (!tunit) {
    if(DEBUG::show({DEBUG::COMPILATION}))
      cerr << file.get_data().description() << ": clang_parseTranslationUnit() failed" << endl;
    return false;
  }

  int errors = clang_getNumDiagnostics(tunit);
  if(errors > 0) {
    if(DEBUG::show({DEBUG::COMPILATION})) {
      cout << file.get_data().description() << ": compilation errors, starting with: " << endl;
      CXDiagnostic diag = clang_getDiagnostic(tunit, 0);
      CXString diag_str = clang_formatDiagnostic(
          diag, clang_defaultDiagnosticDisplayOptions());
      cout << clang_getCString(diag_str) << endl;
      clang_disposeString(diag_str);
    }
  
    clang_disposeTranslationUnit(tunit);
    clang_disposeIndex(index);
    return false;
  }

	CXFile files = clang_getFile(tunit, file_path);
	CXSourceLocation begin = clang_getLocationForOffset(tunit, files, 0);
	CXSourceLocation end = clang_getLocationForOffset(tunit, files,
	    boost::filesystem::file_size(file_path));

  CXToken* tokens;
  unsigned num_tokens;
  clang_tokenize(tunit, clang_getRange(begin, end), &tokens, &num_tokens);

  CXCursor cursors[num_tokens];
  clang_annotateTokens(tunit, tokens, num_tokens, cursors);
	
  if(DEBUG::show({DEBUG::COMPILATION}))
    cerr << file.get_data().description() << ": " << num_tokens << " token(s) found" << endl;

  for(int i = 0; i < num_tokens; ++i) {

    CXCursor cursor = cursors[i];
    CXString cxtoken = clang_getTokenSpelling(tunit, tokens[i]);
    string token_string = clang_getCString(cxtoken);
    clang_disposeString(cxtoken);
	  string token;

	  switch (clang_getTokenKind(tokens[i])) {
	    case CXToken_Punctuation:
	      token = token_string;
	      break;

      case CXToken_Keyword:
        {
          token = token_string;

          CXString next_cxtoken = clang_getTokenSpelling(tunit, tokens[i+1]);
          string next_token = clang_getCString(next_cxtoken);
          clang_disposeString(next_cxtoken);
          if(is_basic_type(token_string) && is_basic_type(next_token))
            token = "";
        }
        break;

      case CXToken_Identifier:
        {
          CXCursorKind kind = clang_getCursorKind(cursor);
          if(clang_isReference(kind) || kind == CXCursor_DeclRefExpr || 
              kind == CXCursor_MemberRefExpr) {
            cursor = clang_getCursorReferenced(cursor);
            kind = clang_getCursorKind(cursor);
          }

          CXFile cxfile;
          uint line = 0, col = 0, offset = 0;
          clang_getSpellingLocation(clang_getCursorLocation(cursor),
              &cxfile, &line, &col, &offset);
          CXString cxfilename = clang_getFileName(cxfile);
          if((clang_getCString(cxfilename) != 0) &&
              (clang_getCString(cxfilename) != string(file_path))) {
            token = token_string;
            clang_disposeString(cxfilename);
            break;
          }
          clang_disposeString(cxfilename);
         
          switch (kind) {
            case CXCursor_StructDecl:
            case CXCursor_ClassDecl:
            case CXCursor_UnionDecl:
            case CXCursor_EnumDecl:
            case CXCursor_TypedefDecl:
            case CXCursor_TemplateTypeParameter:
            case CXCursor_ClassTemplate:
            case CXCursor_TemplateTemplateParameter:
            case CXCursor_ClassTemplatePartialSpecialization:
              token = "1";
              break;
            
            case CXCursor_FieldDecl:
            case CXCursor_EnumConstantDecl: 
            case CXCursor_VarDecl:
            case CXCursor_ParmDecl:
            case CXCursor_NonTypeTemplateParameter:
              token = "2";
              break;
              
            case CXCursor_FunctionDecl:
            case CXCursor_CXXMethod:
            case CXCursor_FunctionTemplate:
              token = "3";
              break;

            case CXCursor_Namespace:
              token = token_string;
              break;

            case CXCursor_Destructor:
              token = "4";
              break;

            case CXCursor_Constructor:
              token = "5";
              break;

            case CXCursor_ConversionFunction:
              token = "6";
              break;

            case CXCursor_PreprocessingDirective:
              {
                CXString prev_cxtoken = clang_getTokenSpelling(tunit, tokens[i-1]);
                string prev_token = clang_getCString(prev_cxtoken);
                clang_disposeString(prev_cxtoken);
                if(prev_token == "#") {
                  token = "#" + token_string;
                  break;
                }
                token = token_string;
                break;
              }

            default:
              token = to_string(kind);
          }
        }
        break;
      
      case CXToken_Literal:
        if(token_string[0] == '\"')
          token =  "\"";
        else if (token_string[0] == '\'')
          token = "\'";
        else token = "0";
        break;

      case CXToken_Comment:
        break;
    }
    token = get_mapping(token);
    
    file.add_token(token_string, DataConverter::hash(token));
  }
  if(DEBUG::show())
    cout << endl << endl;

  clang_disposeTokens(tunit, tokens, num_tokens);
  clang_disposeTranslationUnit(tunit);
  clang_disposeIndex(index);
  return true;
}

string FilePreprocessor::get_mapping(const string& token) {
  if(mapping.find(token) != mapping.end())
    return mapping.find(token)->second;
  return token;
}

bool FilePreprocessor::is_basic_type(const string& token) {
  if(get_mapping(token) == get_mapping(string("int")) ||
      get_mapping(token) == get_mapping(string("float")))
    return true;
  return false;
}

