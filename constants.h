#ifndef MASTERTHESIS_CONSTANTS_H
#define MASTERTHESIS_CONSTANTS_H

#include <gflags/gflags.h>

//Init:
void init(int argc, char* argv[]);

//Parsing flags:
struct DEBUG {
  static const std::string ALL;
  static const std::string SATORI;
  static const std::string FILE_DATA;
  static const std::string COMPILATION;

  static bool show(
      const std::vector<std::string> PARAMS = std::vector<std::string>());
};

std::vector<std::pair<std::string, std::string>> parse_problems();

//Mode:
DECLARE_bool(satori);
DECLARE_bool(stdin);
DECLARE_string(athina);

DECLARE_int32(maxlen);
DECLARE_string(database);
DECLARE_string(contest);
DECLARE_string(problem);
DECLARE_string(user);
DECLARE_string(submits);
DECLARE_string(cxxflags);
DECLARE_int32(start);
DECLARE_int32(progress);
DECLARE_int32(rank);
DECLARE_int64(maxscore);
DECLARE_string(admins);
DECLARE_string(status_regex);
DECLARE_string(debug);

DECLARE_bool(extending);
DECLARE_bool(mismatch);
DECLARE_bool(onlyacc);
DECLARE_bool(onlylast);
DECLARE_bool(cpp0x);

typedef long long ll;
typedef unsigned int uint;

const std::string INSTRUCTIONS = "\nInstructions:\n \
p 'x' - display comparison between files from the ranking line number 'x' (graphical mode)\n \
f 'x' 'y' - find the rank of the pair of the files with ids 'x' and 'y'\n \
r 'x' - print only ranking lines with the file with the id 'x'\n \
u - print ranking of all the pairs with uncommon fits\n \
w - print the whole ranking\n \
i - print instructions\n \
q - quit\n \
\n\
When in the comparison mode: \n \
↑ / ↓ / PgUp / PgDn - scroll the panels that have focus\n \
← / → - set the focus to the left / right panel\n \
Space - set the focus to both panels\n \
w - display all whitespace\n \
q - quit\n\n";

#endif

