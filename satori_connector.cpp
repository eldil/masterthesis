#include "satori_connector.h"
#include "constants.h"
#include "data_converter.h"
#include <regex>
#include <boost/filesystem.hpp>

using namespace boost;
using namespace boost::filesystem;
using namespace satori;
using namespace std;

SatoriConnector::SatoriConnector() : initialized(false) {
}

bool SatoriConnector::initialize() {
  Connection::Configuration config;
  if(!initialized) {
    connection.reset(new Connection(config));
  }

  auto protocol = connection->connect_thrift();
  contest_client.reset(new ContestClient(protocol));
  contestant_client.reset(new ContestantClient(protocol));
  submit_client.reset(new SubmitClient(protocol));
  problem_client.reset(new ProblemMappingClient(protocol));
  token = connection->get_token();

  create_directory(FLAGS_submits);
  initialized = true;
  return true;
}

void SatoriConnector::get_files(std::vector<FileData>& output) {
  output = vector<FileData>(files);
}

void SatoriConnector::sort_files() {
  sort(files.begin(), files.end(), FileData::sort_by_kind);
}

bool SatoriConnector::add_files(
    const vector<pair<string, string>>& problems,
    const string user) {
  
  if(!initialized)
    throw logic_error("SatoriConnector not initialized!");

  vector<FileData> temp_files;

  for(int i = 0; i < 5; ++i) {
    try {
      cout << "Downloading submits:" << endl;
      time_t start_time = time(0);

      for(auto problem: problems)
        add_files_for_problem(problem.first, problem.second, temp_files);
      
      files.insert(files.end(), temp_files.begin(), temp_files.end());
      time_t now = time(0);
      cout << endl << "Processing time: " <<
        DataConverter::time_to_string(difftime(now, start_time)) << "." << endl;
      return true;
    
    } catch(apache::thrift::TException ex) {
      temp_files.clear();
      cout << endl << ex.what() << endl;
      initialize();
    }
  }
  return false;
}

void SatoriConnector::add_files_for_problem(
    const string contest_name,
    const string problem_name,
    vector<FileData>& files) {
 
  cout << "  " << contest_name << ":" << problem_name << endl;
  regex status_regex(FLAGS_status_regex);
  
  ContestStruct contest_filter;
  contest_filter.__set_name(contest_name);
  vector<ContestStruct> contests;
  contest_client->Contest_filter(contests, token, contest_filter);
  if(contests.size() == 0)
    return;
  ContestStruct contest = contests[0];

  ProblemMappingStruct problem_filter;
  problem_filter.__set_contest(contest.id);
  if (!problem_name.empty())
    problem_filter.__set_code(problem_name);
  vector<ProblemMappingStruct> problems;
  problem_client->ProblemMapping_filter(problems, token, problem_filter);
  for(auto& problem: problems) {

    SubmitStruct submit_filter;
    submit_filter.__set_problem(problem.id);
    vector<SubmitStruct> submits;
    submit_client->Submit_filter(submits, token, submit_filter);

    for(auto& submit: submits) {
        
      //TODO:onlylast

      ContestantStruct contestant;
      contestant_client->Contestant_get_struct(contestant, token, submit.contestant);
      if(FLAGS_admins.find(":" + contestant.name + ":") != string::npos) {
        if(DEBUG::show({DEBUG::SATORI}))
          cerr << submit.id << " skipped, user '" << contestant.name << "' is admin" << endl;
        continue;
      }
      
      map<string, AnonymousAttribute> submit_data;
      submit_client->Submit_data_get_map(submit_data, token, submit.id);
      string filename = submit_data["content"].filename;
      string language = reader.get_language(filename);
      if(language.empty()) {
        if(DEBUG::show({DEBUG::SATORI}))
          cerr << submit.id << " skipped, no language" << endl;
        continue;
      }

      string status;
      submit_client->Submit_get_test_suite_status(
          status, token, submit.id, problem.default_test_suite);
      smatch regex_match;
      if (!FLAGS_status_regex.empty() && !regex_search(status, regex_match, status_regex)) {
        if(DEBUG::show({DEBUG::SATORI}))
          cerr << submit.id << " skipped, status does not match regex" << endl;
        continue;
      }
        
      string dest_filepath = (FLAGS_submits/to_string(submit.id)).string();
      if(exists(dest_filepath)) { //we assume that submit.id is unique
        if(DEBUG::show({DEBUG::SATORI}))
          cerr << submit.id << " already downloaded" << endl;
      } else {
        int result = connection->download_blob(dest_filepath, "Submit", submit.id, "data", "content");
        if(!result) {
          if(DEBUG::show({DEBUG::SATORI}))
            cerr << submit.id << " skipped, download failed" << endl;
          continue;
        }
        if(DEBUG::show({DEBUG::SATORI}))
          cerr << submit.id << " downloaded" << endl;
      }

      FileData file_data;
      file_data.file_path = dest_filepath;
      file_data.language = language;
      file_data.id = to_string(submit.id);
      file_data.contest = contest_name;
      file_data.problem = problem.code;
      file_data.user = contestant.name;

      //cout << file_data.description() << ' ' << problem_mapping.code << endl;
      files.push_back(file_data);
    }
  }
}

