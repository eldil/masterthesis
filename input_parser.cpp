#include "input_parser.h"
#include "file_data.h"
#include "file_reader.h"
#include <iostream>
#include <boost/filesystem.hpp>
using namespace boost::filesystem;
using namespace std;

void InputParser::read(vector<FileData>& files) {
  cout << "Provide paths in consecutive lines, ending with an empty line:\n";
  cin.getline(line, N);
  path p = path(line);
  FileReader reader;
  while(!p.empty()) {
    if(!is_regular_file(p))
      cout << "File doesn't exist!\n";
    else {
      string language = reader.get_language(p.c_str());
      if(!language.empty()) {
        FileData data;
        data.file_path = p;
        data.language = language;
        data.id = p.c_str();
        files.push_back(data);
      }
    }
    cin.getline(line, N);
    p = path(line);
  }
}

