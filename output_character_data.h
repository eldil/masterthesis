#ifndef MASTERTHESIS_OUTPUT_CHARACTER_DATA_H
#define MASTERTHESIS_OUTPUT_CHARACTER_DATA_H

struct OutputCharacterData {
  
  OutputCharacterData(char character, int row, int col, int color) : 
    character(character), row(row), col(col), color(color) {
    }
  
  char character;
  int row;
  int col;
  int color;

};

#endif

