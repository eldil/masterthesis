#ifndef MASTERTHESIS_INPUT_PARSER_H
#define MASTERTHESIS_INPUT_PARSER_H

#include "file_data.h"
#include <vector>

class InputParser {
 
 public:
  
  void read(std::vector<FileData>& files);

private:

  static const int N = 1024;

  char line[N];

};

#endif

