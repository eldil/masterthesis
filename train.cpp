#include "constants.h"
#include "input_parser.h"
#include "file_searcher.h"
#include "satori_connector.h"
#include "database.h"
using namespace std;

int main(int argc, char* argv[]) {

  init(argc, argv);

  vector<FileData> files;
  if(FLAGS_stdin) {
    InputParser parser;
    parser.read(files);
  } else if(FLAGS_satori) {
    if(FLAGS_contest.empty() && FLAGS_problem.empty()) {
      cout << "Please provide contest: --contest=\"c1,c2,...\" or problem=\"c1:p1,c2:p2,...\n";
      return 0;
    }
    SatoriConnector connector;
    connector.initialize();
    connector.add_files(parse_problems());
    connector.sort_files();
    connector.get_files(files);
  } else { 
    if(FLAGS_athina.empty()) {
      cout << "Usage: --stdin, --satori or --athina=\"path\"\n";
      return 0;
    }
    FileSearcher searcher;
    if(FLAGS_contest.empty()) searcher.add_athina_files(path(FLAGS_athina));
    else searcher.add_athina_files(path(FLAGS_athina), FLAGS_contest); 
    searcher.sort_files();
    searcher.get_files(files);
  }

  /*FileData data;
  data.file_path = "athina/ACS1_0809/server/contest/data/data/6/14986";
  data.language = "c++";
  data.id = basename(data.file_path);
  files.clear();
  files.push_back(data);*/

  Database db;
  db.initialize(FLAGS_database);
  db.add_train_data(files);
}

