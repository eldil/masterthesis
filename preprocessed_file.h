#ifndef MASTERTHESIS_PREPROCESSED_FILE_H
#define MASTERTHESIS_PREPROCESSED_FILE_H

#include "constants.h"
#include "file_data.h"
#include "file_reader.h"
#include <string>
#include <vector>

struct Token {

 public:

  Token(uint v, int s, int e) : value(v), start(s), end(e) {}
  
  uint value;
  int start, end;
};

class PreprocessedFile {

 public:
 
  PreprocessedFile() : last_token_end(0) {}

  void initialize(const FileData& data);

  void add_token(const std::string& token_string, uint token);

  int get_text_size() const;

  const FileData& get_data() const;
  const boost::filesystem::path& get_path() const;
  const std::string& get_language() const;
  const std::string& get_user() const;
  const std::vector<Token>& get_tokens() const;
  const std::string& get_text() const;
  
 private:

  static FileReader reader;

  FileData data;
  std::vector<Token> tokens;
  std::string text;
  int last_token_end;

};

#endif

