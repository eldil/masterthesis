#include "file_data.h"
using namespace std;

bool FileData::sort_by_kind(const FileData& first, const FileData& second) {
  if(first.contest != second.contest)
    return first.contest < second.contest;
  if(first.problem != second.problem)
    return first.problem < second.problem;
  return first < second;
}

string FileData::description(bool add_user) const {
  string add;
  if(add_user && !user.empty())
    add = " (" + user + ")";
  if(!contest.empty() && !problem.empty())
    return contest + "/" + problem + "/" + id + add;
  return id + add;
}

bool operator< (const FileData& first, const FileData& second) {
  int nr1 = -1, nr2 = -1;
  try {
    nr1 = stoi(first.id);
    nr2 = stoi(second.id);
  } catch(...) {
  }
  if(nr1 == -1 || nr2 == -1)
    return first.id < second.id;
  return nr1 < nr2;
}

