# adjust these as necessary:
THRIFT_DIR := /usr/local
CLANG_DIR := /usr/lib/llvm-3.4

all:
	$(MAKE) satori-api
	$(MAKE) train test

CPP := $(wildcard *.cpp)
OBJ := $(patsubst %.cpp,%.o,${CPP})

API_CPP := $(wildcard satori-api/*.cpp)
API_OBJ := $(patsubst %.cpp,%.o,$(API_CPP))

CPPFLAGS := -I${CLANG_DIR}/include -I/usr/include/boost -DBOOST_NO_CXX11_SCOPED_ENUMS
CXXFLAGS := -std=c++0x -O2 -fPIC
LDFLAGS :=	-L${THRIFT_DIR}/lib -lthrift -Wl,-rpath=${THRIFT_DIR}/lib \
			-L${CLANG_DIR}/lib -lclang \
			-lboost_system -lboost_filesystem -lboost_thread -lsqlite3 -lgflags -lcurl

TRAIN_OBJ := $(filter-out output_formatter.o test.o,${OBJ})
TEST_OBJ := $(filter-out train.o,${OBJ})

train : $(TRAIN_OBJ) libsatori.a
	${CXX} $^ ${LDFLAGS} -o $@

test : $(TEST_OBJ) libsatori.a
	${CXX} $^ ${LDFLAGS} -lncurses -o $@

libsatori.a : satori-api $(API_OBJ) connection.o
	ar cru $@ $(API_OBJ) connection.o
	ranlib $@

satori-api: satori.idl
	mkdir $@
	thrift -gen cpp -out $@ $<
	rm satori-api/*.skeleton.*

satori.idl:
	satori.core idl > $@

clean:
	rm -rf libsatori.a satori-api test train $(API_OBJ) $(OBJ)

.PHONY: all clean

