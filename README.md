Seigi - Efficient source code plagiarism detection system by Dominik Dudzik


Before compiling the program, check (and provide, if necessary) the library paths in the Makefile (libraries needed: boost (boost filesystem, boost system, boost thread, boost regex), sqlite3, ncurses, clang, gflags).


I. To compile the whole project:

make


II. To add files to the database (providing filepaths on the standard input):

./train --stdin


III. To check a group of files for plagiarism (providing filepaths on the standard input):

./test --stdin


IV. Additional parameters to the program can be passed using gflags. Basic usage: –variable=value for non-boolean flags, and –variable/–novariable for boolean flags. The following variables are available:
  
  maxlen (default 10) - maximum length of sequences of tokens added/selected from the database (L)
  
  database - path to the database used
  
  start - the starting number of the file for training - building database (in case the program execution breaks, we can restart it with this parameter specified. If we do not know even approximately at which number it broke, we can also start it from the very beginning - files that have already been added will be discarded anyway, thanks to the content hashing).
  
  progress (10) - the frequency, the information about training/testing is displayed with
  
  rank (100) - number of rows of the plagiarism ranking that are printed
  
  maxscore (1000) - maximum score for one match (due to the scoring function used, this is equal V2 and not V)
  
  debug (false) - print detailed information about the program execution
  
  stdin (false) - file paths provided on the stdin in separate lines, ending with an empty line
  
  extending (true) - use match extending
  
  mismatch (true) - extend matches allowing small mismatches
  
  cpp0x (false) - files should be parsed with -stc=c++0x option


V. To use the program with the archive data of the Athina System:

./train --athina=DirectoryPatch (--contest=ContestRegexp)

./test --athina=DirectoryPath --contest=Contest --problem=Problem


Additional flags:

  admins (:admin:) - users which are admins (their submissions ignored by the system), separated by ’:’
  
  onlyacc (false) - while iterating the athina files, only the files that received status “OK” are considered
  
  onlylast (false) - while iterating the athina files, only the last file by each user is considered


VI. Colors:

In the comparison mode, characters are colored based on the match score of the sequences they constitute. If there are many sequences they belong to, the maximum score is assigned. Uncommon literal fits are always displayed with red font on yellow. Score levels are as follows (from the lowest to the highest):

1. black on white

2. green on white

3. blue on white

4. black on green

5. blue on green

6. yellow on blue

7. white on blue

8. black on red

9. white on red

