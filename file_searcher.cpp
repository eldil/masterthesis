#include "file_searcher.h"
#include "constants.h"
#include <cstdlib>
#include <regex>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/lexical_cast.hpp>
using namespace boost;
using namespace boost::filesystem;
using namespace boost::algorithm;
using namespace std;

FileSearcher::FileSearcher() {
 reset();
}

void FileSearcher::set_backup(const path& dest_dir, int frequency) {
  this->dest_dir = dest_dir;
  this->frequency = frequency;
}

void FileSearcher::backup_data() {
  cout << "Copying " << (files.size() - last_backup - 1) <<
    " files....................................." << endl;
  copy_files(last_backup + 1, files.size());
  last_backup = files.size() - 1;
  cout << "Copying completed." << endl;
}

void FileSearcher::get_files(vector<FileData>& output) {
  output = vector<FileData>(files);
}

void FileSearcher::sort_files() {
  sort(files.begin(), files.end(), FileData::sort_by_kind);
}

bool FileSearcher::get_file_text(int nr, string& output) {
  if(nr >= files.size()) {
    return false;
  }
  path p = files[nr].file_path;
  reader.read_file(p, output);
  return true;
}

void FileSearcher::add_athina_files(const path& athina_path,
    const string contest_regex, const string problem_name) {
  
  for(directory_iterator contest_iter(athina_path);
      contest_iter != directory_iterator(); ++contest_iter) {

    string contest = basename(contest_iter->path());
    if(!regex_match(contest.c_str(), regex(contest_regex.c_str())))
      continue;
    path contest_path = contest_iter->path()/"server/contest";
    
    if(is_directory(contest_path)) {
    int prev = files.size();
      
      for(int i = 0; i < 10; ++i) {
        path files_dir_path = contest_path/"data/data"/lexical_cast<string>(i);
        
        for(directory_iterator file_iter(files_dir_path);
            file_iter != directory_iterator(); ++file_iter) {

          path p = file_iter->path();
          string file = lexical_cast<string>(i) + "/" + basename(p);

          string filename, user, status, problem;
          reader.read_file(contest_path/"filename/filename"/file, filename);
          reader.read_file(contest_path/"user/user"/file, user);
          while((user.size() > 0) and (user[user.size() -1] == char(NULL)))
            user.erase(user.size() - 1);
          reader.read_file(contest_path/"status/status"/file, status);
          reader.read_file(contest_path/"problem/problem"/file, problem);
          string language = reader.get_language(filename);
         
          if(!problem_name.empty() and ((problem_name != problem)))
            continue;
          if(language.empty())
            continue;
          if(FLAGS_admins.find(":" + user + ":") != string::npos)
            continue;
          if(FLAGS_onlyacc and (status != string("OK")))
            continue;
          if(!FLAGS_onlyacc and (status == string("CME")))
            continue;
          
          FileData file_data;
          file_data.file_path = p;
          file_data.language = language;
          file_data.id = basename(p);
          file_data.contest = contest;
          file_data.problem = problem;
          file_data.user = user;

          string userId = contest + ":" + problem + ":" + user;
          if(FLAGS_onlylast and(users.find(userId) != users.end())) {
            if(files[users[userId]] < file_data)
              files[users[userId]] = file_data;
            continue;
          }
            
          users[userId] = files.size();
          files.push_back(file_data);
        }
      }
      cout << contest << ": " << files.size()-prev << endl;
    }
  }
  cout << "TOTAL NUMBER OF FILES: " << files.size() << endl << endl;
}

void FileSearcher::copy_files(int start, int end) {
  start = max(0, start);
  end = min(end, (int)files.size());
  
  for(int i = start; i < end; ++i) {
    string file_name = files[i].contest + "/" + files[i].problem + 
      "/" + files[i].user + "/" + files[i].id;
    path dest = dest_dir/file_name;
    copy_file(files[i].file_path, dest);
  }
}

void FileSearcher::reset() {
  frequency = 0;
  last_backup = -1;
  files.clear();
  users.clear();
}

