#include "output_formatter.h"
#include "constants.h"
#include <boost/algorithm/string.hpp>
#include <cmath>
using namespace std;
using namespace boost;

#define WIDTH (COLS/2-2)

OutputFormatter::OutputFormatter() {
  initscr(); // Start curses mode
  if(has_colors() == false) {
    cout << "Your terminal does not support color\n";
    exit(1);
  }
  start_color(); // Start color
  noecho();
  keypad(stdscr, true);
  refresh();
  
  init_pair(1, COLOR_BLACK, COLOR_WHITE);
  init_pair(2, COLOR_GREEN, COLOR_WHITE);
  init_pair(3, COLOR_BLUE, COLOR_WHITE);
  
  init_pair(4, COLOR_BLACK, COLOR_GREEN);
  init_pair(5, COLOR_BLUE, COLOR_GREEN);

  init_pair(6, COLOR_YELLOW, COLOR_BLUE);
  init_pair(7, COLOR_WHITE, COLOR_BLUE);
  
  init_pair(8, COLOR_BLACK, COLOR_RED);
  init_pair(9, COLOR_WHITE, COLOR_RED);

  init_pair(10, COLOR_RED, COLOR_YELLOW);
}

OutputFormatter::~OutputFormatter() {
  endwin();
}

void OutputFormatter::display_files(const PreprocessedFile& first,
    const PreprocessedFile& second, const vector<int>& first_details,
    const vector<int>& second_details) {

  WINDOW* first_pad = newpad(1000, WIDTH);
  WINDOW* second_pad = newpad(1000, WIDTH);

  int end[2];
  end[0] = get_file_data(first.get_text(), first_details,
      first_characters);
  display_file(first_characters, first_pad);
  end[1] = get_file_data(second.get_text(), second_details,
      second_characters);
  display_file(second_characters, second_pad);

  prefresh(first_pad, 0, 0, 0, 0, LINES-1, COLS/2-2);
  prefresh(second_pad, 0, 0, 0, WIDTH+4, LINES-1, COLS-1);

  bool whitespace_visible = false;
  int command, column = 2;
  int start[] = {0, 0};
  while((command = getch()) != 'q' && command != KEY_EXIT) {
    switch (command) {
      case KEY_LEFT:
        column = 0;
        break;
      case KEY_RIGHT:
        column = 1;
        break;
      case ' ':
        column = 2;
        break;
      case 'w':
        whitespace_visible = not whitespace_visible;
        display_file(first_characters, first_pad, whitespace_visible);
        display_file(second_characters, second_pad, whitespace_visible);
        break;
      case KEY_UP:
        change_row(start, end, column, -1);
        break;
      case KEY_DOWN:
        change_row(start, end, column, 1);
        break;
      case KEY_PPAGE:
        change_row(start, end, column, -LINES);
        break;
      case KEY_NPAGE:
        change_row(start, end, column, LINES);
        break;
    }
    if(column != 1)
      prefresh(first_pad, start[0], 0, 0, 0, LINES-1, WIDTH);
    if(column != 0)
      prefresh(second_pad, start[1], 0, 0, WIDTH+4, LINES-1, COLS-1);
  }
  delwin(first_pad);
  delwin(second_pad);
}

void OutputFormatter::change_row(int start[], int end[], int column, int value) {
  if(column != 1)
    start[0] = min(max(0, start[0] + value), end[0] - LINES);
  if(column != 0)
    start[1] = min(max(0, start[1] + value), end[1] - LINES);
}

int OutputFormatter::get_file_data(const string& file,
    const vector<int>& details, vector<OutputCharacterData>& data) {

  data.clear();
  int col = 0, row = 0;
  
  for(int i = 0; i < details.size(); ++i) {
    if(col == WIDTH) {++row; col = 0;}
    OutputCharacterData character_data(file[i], row, col++,
        get_color(details[i]));
    data.push_back(character_data);
    if(file[i] == '\n') {++row; col = 0;}
    else if(file[i] == '\t') {
      if(col == WIDTH) {++row; col = 0;}
      OutputCharacterData character_data(char(NULL), row, col++,
          get_color(details[i]));
      data.push_back(character_data);
    }
  }
  return row + 1;
}

void OutputFormatter::display_file(const vector<OutputCharacterData>& data,
    WINDOW* pad, bool whitespace_visible) {

  werase(pad);
  for(auto it = data.begin(); it != data.end(); ++it) {
    char token = it->character;
    if(whitespace_visible) {
      if(token == '\n')
        token = '$';
      else if(token == ' ')
        token = '~';
      else if(token == '\t')
        token = '=';
    }
    if(token == char(NULL))
      token = ' ';
    wattron(pad, COLOR_PAIR(it->color));
    mvwaddch(pad, it->row, it->col, token);
    wattroff(pad, COLOR_PAIR(it->color));
  }
}

int OutputFormatter::get_color(int score) {
  const int MAX = FLAGS_maxscore;
  if(score > MAX) return 10;

  if(score == MAX) return 9;
  if(score == MAX - 1) return 8;

  if(score >= MAX * 19 / 20) return 7;
  if(score >= MAX * 9 / 10) return 6;
  if(score >= MAX * 2 / 3) return 5;

  if(score >= MAX / 2) return 4;
  if(score >= MAX / 3) return 3;
  if(score >= MAX / 4) return 2;
  if(score >= 1) return 1;
  return 0;
}

