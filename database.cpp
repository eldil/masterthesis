#include "database.h"
#include "constants.h"
#include "data_converter.h"
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <boost/algorithm/string.hpp>
using namespace std;
using namespace boost;

Database::Database() : error_message(0), db(NULL) {
}

Database::~Database() {
  sqlite3_close(db);
}

bool Database::initialize(string name) {
  if(sqlite3_open(name.c_str(), &db)) {
    cout << "Can't open database: " <<  sqlite3_errmsg(db);
    sqlite3_close(db);
    return false;
  }
  const char* create_tables = "CREATE TABLE files (hash TEXT PRIMARY KEY, \
    path TEXT, language TEXT, id TEXT, user TEXT, contest TEXT, problem TEXT);\
    CREATE TABLE frequency (string BLOB PRIMARY KEY, count INT);";
  sqlite3_exec(db, create_tables, 0, 0, 0);
  return true;
}

void Database::add_train_data(const vector<FileData>& data) {
  
  time_t start = time(0);
  int accepted = 0;
  cout << "PROCESSING FILES\n";

  for(int i = FLAGS_start; i < data.size(); ++i) {  
    PreprocessedFile file;
    file.initialize(data[i]);
    if(add_file(file) and preprocessor.process(file)) {
      ++accepted;
      add_train_data(file);
    }

    if((i-FLAGS_start) % FLAGS_progress == FLAGS_progress-1) {
      
      cout << i-FLAGS_start+1 << "/" << data.size()-FLAGS_start
        << " files (" << accepted << " accepted)" <<  endl;
      cout << data[i-FLAGS_progress+1].description(false) << " - " <<
        data[i].description(false) << endl;
      
      time_t now = time(0);
      long long expect = ((long long)data.size() - i) * difftime(now, start) /
        (i-FLAGS_start);
      cout << "Processing time: " << DataConverter::
        time_to_string(difftime(now, start)) << ". Estimated time left: " <<
        DataConverter::time_to_string(expect) << endl;
    }
  }
  cout << data.size()-FLAGS_start << "/" << data.size()-FLAGS_start
    << " files (" << accepted << " accepted)" <<  endl;
  time_t end = time(0);
  cout << "TOTAL PROCESSING TIME: " <<
    DataConverter::time_to_string(difftime(end, start)) << endl;
}

bool Database::add_file(const PreprocessedFile& file) {
  
  string hash = DataConverter::md5(file.get_text());
  char *zSQL = sqlite3_mprintf("INSERT INTO files VALUES (%Q, %Q, %Q, %Q, %Q, \
       %Q, %Q);", hash.c_str(), file.get_path().c_str(), file.get_language().
      c_str(), file.get_data().id.c_str(), file.get_user().c_str(),
      file.get_data().contest.c_str(), file.get_data().problem.c_str()); 
  int result = sqlite3_exec(db, zSQL, 0, 0, &error_message);
  sqlite3_free(zSQL); 
  
  if(result == SQLITE_CONSTRAINT) {
    cout << "File " << file.get_data().description() <<
      " already in the database! (hash: " << hash << " )\n";
    sqlite3_free(error_message);
    return false;
  }
  else if(result != SQLITE_OK) {
    cout << "SQL error: " << ' ' << error_message << endl;
    sqlite3_free(error_message);
    return false;
  }
  return true;
}

void Database::add_train_data(const PreprocessedFile& file) { 

  sqlite3_exec(db, "BEGIN;", 0, 0, 0); 

  char buffer[] = "INSERT OR REPLACE INTO frequency VALUES (?1, (COALESCE \
    ((SELECT count FROM frequency WHERE string=?1), 0)+1));";
  sqlite3_stmt* stmt;
  sqlite3_prepare_v2(db, buffer, strlen(buffer), &stmt, NULL);
  
  const vector<Token>& tokens = file.get_tokens();
  vector<uint> sub;
  string sub_str;
  
  for(int i = 0; i < tokens.size(); ++i) {
    sub.clear();
    
    for(int j = i; j < min(int(tokens.size()), i + FLAGS_maxlen); ++j){
      sub.push_back(tokens[j].value);
      DataConverter::encode(sub, sub_str);
      sqlite3_bind_blob(stmt, 1, sub_str.c_str(), sub_str.size(),
          SQLITE_STATIC);
      if(sqlite3_step(stmt) != SQLITE_DONE)
        cout << "Commit Failed!\n";
      sqlite3_reset(stmt);
    }
  }
  sqlite3_finalize(stmt);
  
  if(sqlite3_exec(db, "COMMIT;", 0, 0, &error_message) != SQLITE_OK) {
    cout << "SQL error: " << error_message << endl;
    sqlite3_free(error_message);
  }
}

void Database::add_test_data(const vector<FileData>& data) {
 
  frequency.clear();
  files.clear();
  mutual.clear();
  matches.clear();

  time_t start = time(0);
  int accepted = 0;
  cout << "PROCESSING FILES\n";

  ll size = 0;
  for(int i = 0; i < data.size(); ++i) {
    PreprocessedFile file;
    file.initialize(data[i]);
    if(preprocessor.process(file)) {
      add_test_data(file);
      ++accepted;
      
/*      if(accepted%100 == 0)
        cout << accepted << ": " << size/accepted+1 << endl;
      size += file.get_tokens().size();
*/    }
    
    if(i % FLAGS_progress == FLAGS_progress-1) {
      cout << i+1 << "/" << data.size() << " files (" << accepted <<
        " accepted)" <<  endl;
    }
  }
//  cout << "AVG: " << size/accepted << endl;
  time_t p_end = time(0);
  cout << data.size() << "/" << data.size() << " files (" << accepted <<
    " accepted)\n" << "TOTAL PROCESSING TIME: " <<
    DataConverter::time_to_string(difftime(p_end, start)) << endl;
  
  cout << "\nGRADING\n";
  grade_data();
  time_t g_end = time(0);
  cout << "TOTAL GRADING TIME: " <<
    DataConverter::time_to_string(difftime(g_end, p_end)) << endl << endl;
}

void Database::add_test_data(const PreprocessedFile& file) {

  int file_nr = files.size();
  files.push_back(file);
  const vector<Token>& tokens = files.back().get_tokens();
  vector<uint> sub;
  string sub_str;

  for(int i = 0; i < tokens.size(); ++i) {
    sub.clear();
    
    for(int j = i; j < min(int(tokens.size()), i + FLAGS_maxlen); ++j){
      sub.push_back(tokens[j].value);
      DataConverter::encode(sub, sub_str);
      auto occurence = make_pair(file_nr, i);
      mutual[sub_str].push_back(occurence);
    }
  }
}

void Database::grade_data() {

  sqlite3_exec(db, "BEGIN;", 0, 0, 0); 
  
  char buffer[] = "SELECT count FROM frequency WHERE string=?1;";
  sqlite3_stmt* stmt;
  sqlite3_prepare_v2(db, buffer, strlen(buffer), &stmt, NULL);

  int count = 0, all = mutual.size();

  for(auto it = mutual.begin(); it != mutual.end(); ++it, ++count) {
    const string& str = it->first;
    vector<uint> sub;
    DataConverter::decode(str, sub);

    sqlite3_bind_blob(stmt, 1, str.c_str(), str.size(), SQLITE_STATIC);
    int result = sqlite3_step(stmt);
    if(result == SQLITE_ROW)
      frequency[str] = sqlite3_column_int(stmt, 0);
    else if(result == SQLITE_DONE)
      frequency[str] = 0;
    else {
      cout << "Select Failed!\n";
      continue;
    }

    int score = get_score(frequency[str]);
    if(score != 0) {
      auto& occurences = it->second;
      
      for(auto ait = occurences.begin(); ait != occurences.end(); ++ait) 
        for(auto bit = occurences.begin(); bit != ait; ++bit) {
          if(ait->first == bit->first)
            continue;
          PreprocessedFile& first = files[ait->first];
          PreprocessedFile& second = files[bit->first];
          
          if(first.get_user().empty() or second.get_user().empty() or
              first.get_user() != second.get_user()) {
            int result = score;
            auto match = extend_match(first.get_tokens(), ait->second,
                second.get_tokens(), bit->second, sub.size(), &result);
            FilesNumbersPair files_pair = make_pair(ait->first, bit->first);
    
            vector<Fit> fits;
            add_literal_fits(fits, match.first.first, match.first.second,
                match.second, first, second);
            MatchDetails match_details(score, result, match, (fits.size() > 0));
            matches[files_pair].push_back(match_details);
          }
        }
    }
    if((count+1) % 10000 == 0)
      cout << count+1 << "/" << all << endl;
    sqlite3_reset(stmt);
  }
  cout << all << "/" << all << endl;
  sqlite3_finalize(stmt);

  if(sqlite3_exec(db, "COMMIT;", 0, 0, &error_message) != SQLITE_OK) {
    cout << "SQL error: " << error_message << endl;
    sqlite3_free(error_message);
  }
}

MatchDetails::Match Database::extend_match(const vector<Token>& first_tokens,
    int first_start, const vector<Token>& second_tokens, int second_start,
    int token_size, int* result) {

  if(!FLAGS_extending)
    return make_pair(make_pair(first_start, second_start), token_size);

  int bonus = max(1, *result/token_size), error = -1;
  int i = first_start - 1, j = second_start - 1;
  for(; i >= 0 and j >= 0; --i, --j) {
    if(first_tokens[i].value != second_tokens[j].value) {
      if(FLAGS_mismatch and (error == -1))
        error = i;
      else break;
    } else error = -1;
    if(error != i)
      *result += bonus;
    //bonus *= (bonus == 1) ? 1 : 0.9;
  }
  ++i; ++j;
  if(error == i) {
    ++i; ++j;
  }

  error = -1;
  int k  = first_start + token_size, l = second_start + token_size;
  for(; k < first_tokens.size() and l < second_tokens.size(); ++k, ++l) {
    if(first_tokens[k].value != second_tokens[l].value) {
      if(FLAGS_mismatch and (error == -1))
        error = k;
      else break;
    } else error = -1;
    if(error != k)
      *result += bonus;
    //bonus *= (bonus == 1) ? 1 : 0.9;
  }
  if(error == k-1) {
    --k; --l;
  }
  return make_pair(make_pair(i, j), k-i);
}

void Database::rank_data(vector<RankData>& output) {
  
  rank.clear();
  output.clear();

  map<FilesNumbersPair, bool> uncommon;

  for(auto& pair_matches : matches) {
    ll result = 0;
    bool is_uncommon = false;
    for(MatchDetails match : pair_matches.second) {
      result += match.score;
      if(match.uncommon_fit)
        is_uncommon = true;
    }
    uncommon[pair_matches.first] = is_uncommon;
    rank.push_back(make_pair(result, pair_matches.first));

  }
  sort(rank.begin(), rank.end());
  reverse(rank.begin(), rank.end());
  
  for(auto it = rank.begin(); it != rank.end(); ++it) {
    const FileData& first_file = files[it->second.first].get_data();
    const FileData& second_file = files[it->second.second].get_data();
    output.push_back(RankData(it - rank.begin(), it->first,
          make_pair(first_file, second_file), uncommon[it->second]));
  }
}

void Database::get_details(int rank_nr, PreprocessedFile& first_file,
    PreprocessedFile& second_file, vector<int>& first_details,
    vector<int>& second_details) {

  first_details.clear();
  int first_nr = rank[rank_nr].second.first;
  first_file = files[first_nr];
  first_details.resize(first_file.get_text_size());

  second_details.clear();
  int second_nr = rank[rank_nr].second.second;
  second_file = files[second_nr];
  second_details.resize(second_file.get_text_size());
  
  FilesNumbersPair files_pair = make_pair(first_nr, second_nr);
  int count = 0, all = mutual.size();
  vector<Fit> fits;
  
  for(MatchDetails& match : matches[files_pair]) {    
    add_details(first_details, match.iscore, match.start1,
        match.start1 + match.length, first_file);
    add_details(second_details, match.iscore, match.start2,
        match.start2 + match.length, second_file);
    add_literal_fits(fits, match.start1, match.start2, match.length,
        first_file, second_file);
  }

  for(Fit& fit : fits) {
    for(int i = fit.first.first; i < fit.first.second; ++i)
      first_details[i] = FLAGS_maxscore + 1;
    for(int i = fit.second.first; i < fit.second.second; ++i)
      second_details[i] = FLAGS_maxscore + 1;
  }
}

void Database::add_details(vector<int>& details, int score, int token_start,
    int token_end, const PreprocessedFile& file) {

  const vector<Token>& tokens = file.get_tokens();
  int start = tokens[token_start].start;
  int end = tokens[token_end - 1].end;
  for(int i = start; i < end; ++i)
    details[i] = max(details[i], score);
}

void Database::add_literal_fits(vector<Fit>& fits, int start1, int start2,
    int len, const PreprocessedFile& file1, const PreprocessedFile& file2) {
  
  const vector<Token>& tokens1 = file1.get_tokens();
  const vector<Token>& tokens2 = file2.get_tokens();

  for(int i = start1 + 1, j = start2 + 1; i < start1 + len; ++i, ++j) {
    int s1 = tokens1[i - 1].end, e1 = tokens1[i].start;
    int s2 = tokens2[j - 1].end, e2 = tokens2[j].start;
    const string& str1 = file1.get_text().substr(s1, e1-s1);
    const string& str2 = file2.get_text().substr(s2, e2-s2);
    if(is_uncommon_fit(str1, str2))
      fits.push_back(make_pair(make_pair(s1, e1), make_pair(s2, e2)));
  }
}

bool Database::is_uncommon_fit(const string& inter1, const string& inter2) {
  
  if(inter1 == string("") or inter1 == string(" "))
    return false;
  vector<string> split1, split2;
  split(split1, inter1, is_any_of("\r\n"), token_compress_on);
  split(split2, inter2, is_any_of("\r\n"), token_compress_on);

  if(split1.size() == 1 or split2.size() == 1)
    return inter1 == inter2;
  if(split1[0].length() > 0 and split2[0].length() > 0)
    return true;
  if(split1.back().empty())
    return false;
  if(split1.back().find_first_not_of(" \t") != string::npos)
    return split1.back() == split2.back();
  if(split1.back().find_first_not_of(split1.back()[0]) != string::npos)
    return split1.back() == split2.back();
  return false;
}

int Database::get_score(int freq) {
  if(FLAGS_maxscore / (freq+1) < freq+1)
    return 0;
  return FLAGS_maxscore - (freq+1)*(freq+1);
}

