#ifndef MASTERTHESIS_FILE_READER_H
#define MASTERTHESIS_FILE_READER_H

#include <string>
#include <boost/filesystem.hpp>
using boost::filesystem::path;

class FileReader {

 public:

  bool is_cpp_file(const path& p);

  int get_size(const path& p);

  std::string get_language(const std::string& filename);

  void read_file(const path& p, std::string& output);

 private:


};

#endif

