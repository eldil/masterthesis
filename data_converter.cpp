#include "data_converter.h"
#include "md5.h"
#include <cstring>
#include <sstream>
#include <iomanip>
#include <iostream>
using namespace std;

void DataConverter::encode(const vector<uint>& from, string& to) {
  to.resize(from.size() * sizeof(uint)); 
  memcpy(&to.front(), &from.front(), to.size());
}
  
void DataConverter::decode(const string& from, vector<uint>& to){
  if(from.size() % sizeof(uint) != 0)
    throw "Invalid Encoding!!";
  to.resize(from.size() / sizeof(uint));
  memcpy(&to.front(), &from.front(), from.size());
}

uint DataConverter::hash(const string& s) {
  if(s.empty())
    return 0;
  return (uint)(hash_function(s) % PRIME);
}

string DataConverter::md5(const string& s) {
    MD5 md5 = MD5(s);
    return md5.hexdigest();
}

string DataConverter::time_to_string(int time) {
  string hours = to_string(time / 3600);
  string minutes = to_string((time % 3600) / 60);
  string seconds = to_string(time % 60);
  stringstream stream;
  stream << setfill('0') << setw(2) << hours << ":" << setw(2) << minutes
    << ":" << setw(2) << seconds;
  return stream.str();
}

hash<string> DataConverter::hash_function;

