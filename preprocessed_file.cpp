#include "preprocessed_file.h"
using namespace std;

void PreprocessedFile::initialize(const FileData& data) {
  this->data = data;
  reader.read_file(data.file_path, text);
}

void PreprocessedFile::add_token(const string& token_string, uint token) {
  int start = text.find(token_string, last_token_end);
  last_token_end = start + token_string.length();
  
  if(token != 0)
    tokens.push_back(Token(token, start, last_token_end));
}

int PreprocessedFile::get_text_size() const {
  return text.length();
}

const FileData& PreprocessedFile::get_data() const {
  return data;
}

const path& PreprocessedFile::get_path() const {
  return data.file_path;
}

const string& PreprocessedFile::get_language() const {
  return data.language;
}

const string& PreprocessedFile::get_user() const {
  return data.user;
}

const vector<Token>& PreprocessedFile::get_tokens() const {
  return tokens;
}

const string& PreprocessedFile::get_text() const {
  return text;
}

FileReader PreprocessedFile::reader;

