#include "constants.h"
#include "data_converter.h"
#include "input_parser.h"
#include "file_searcher.h"
#include "satori_connector.h"
#include "output_formatter.h"
#include "database.h"
#include <iostream>
#include <iomanip>
#include <utility>
#include <vector>
#include <boost/algorithm/string.hpp>
using namespace std;
using namespace boost;

void print_pair(RankData data) {
  string first = data.files.first.description();
  string second = data.files.second.description();
  if(!(data.files.first < data.files.second))
    swap(first, second);
  cout << setw(4) << data.rank << "." << setw(15) << data.score <<
    setw(50) << first << setw(50) << second;
  if(data.uncommon_fit)
    cout << setw(10) << 'U';
  cout << endl;
}

int main(int argc, char* argv[]) {

  init(argc, argv);
  
  vector<FileData> files;
  if(FLAGS_stdin) {
    InputParser parser;
    parser.read(files);
  } else if (FLAGS_satori) { 
    if(!FLAGS_contest.empty()) {
      cout << "Does it make any sense to test on a whole contest? Rethink!\n";
      return 0;
    }
    if(FLAGS_problem.empty()) {
      cout << "Please provide problems: --problem=\"c1:p1,c2:p2,...\"\n";
      return 0;
    }
    SatoriConnector connector;
    connector.initialize();
    connector.add_files(parse_problems()); 
    connector.sort_files();
    connector.get_files(files);
  } else {
    if(FLAGS_athina.empty() || FLAGS_contest.empty() ||
        FLAGS_problem.empty()) {
      cout << "Provide the athina folder path (--athina=""), " <<
        "contest (--contest="") and problem name (--problem="")\n";
      return 0;
    }
    FileSearcher searcher;
    searcher.add_athina_files(path(FLAGS_athina), FLAGS_contest, FLAGS_problem);
    searcher.sort_files();
    searcher.get_files(files);
  }

  Database db;
  db.initialize(FLAGS_database); 
  db.add_test_data(files); 
  vector<RankData> rank;
  db.rank_data(rank);

  const int PRINT_MAX = min(FLAGS_rank, int(rank.size()));
  cout << setfill(' ');
  for(int i = 0; i < PRINT_MAX; ++i)
    print_pair(rank[i]);

  FileReader reader;
  PreprocessedFile first_file, second_file;
  vector<int> first_details, second_details;
  cout << INSTRUCTIONS;

  string line;
  while(true) {
    cout << ">> " << flush;
    getline(cin, line);
    algorithm::trim(line);
    
    vector<string> command;
    split(command, line, is_any_of("\t "), token_compress_on);

    if(command.size() < 1) 
      continue;
    
    bool quit = false;
    switch(command[0][0]) {
      case 'p':
        {
          if(command.size() < 2) {
            cout << "Invalid command\n";
            break;
          }
          int pair_nr = -1;
          try {
            pair_nr = stoi(command[1]);
          } catch(...) {
          }
          if((pair_nr < 0) || (pair_nr >= rank.size())) {
            cout << "Invalid line number\n";
          } else {
            //auto file_pair = rank[pair_nr].files;
            db.get_details(pair_nr, first_file, second_file,
                first_details, second_details);
            OutputFormatter of;
            of.display_files(
                first_file, second_file, first_details, second_details);
          }
        }
        break;
      
      case 'f':
        if(command.size() < 3) {
          cout << "Invalid command\n";
          break;
        }

        for(int i = 0; i < rank.size(); ++i) {
          FileData& first = rank[i].files.first;
          FileData& second = rank[i].files.second;
          if((find(command.begin(), command.end(), rank[i].files.first.id) !=
                command.end()) && (find(command.begin(), command.end(),
                  rank[i].files.second.id) != command.end()))
            print_pair(rank[i]);
        }
        break;

      case 'r':
        {
          if(command.size() < 2) {
            cout << "Invalid command\n";
            break;
          }
          int cnt = 0;
          for(int i = 0; i < rank.size() and cnt < FLAGS_rank; ++i)
            if((rank[i].files.first.id == command[1]) or
                (rank[i].files.second.id == command[1])) {
              ++cnt;
              print_pair(rank[i]);
            }
        }
        break;
          
      case 'u':
        {
          int cnt = 0;
          for(int i = 0; i < rank.size() and cnt < FLAGS_rank; ++i)
            if(rank[i].uncommon_fit) {
              ++cnt;
              print_pair(rank[i]);
            }
        }
        break;

      case 'w':
        for(int i = 0; i < PRINT_MAX; ++i)
          print_pair(rank[i]);
        break;

      case 'i':
        cout << INSTRUCTIONS;
        break;

      case 'q':
        quit = true;
        break;

      default:
        cout << INSTRUCTIONS;
    }
    if(quit) break;
  }
}

