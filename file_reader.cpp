#include "file_reader.h"
#include <fstream>
#include <sys/wait.h>
using namespace std;

bool FileReader::is_cpp_file(const path& p) {
  if(file_size(p) < 40 || file_size(p) > 50000)
    return false;
  
  const char* args[] = {"g++", "-fsyntax-only", "-Wfatal-errors",
    "-x" "c++", p.c_str(), NULL};
  int pid = fork();
  if(pid < 0) throw 123;
  if(pid == 0) {
    freopen("/dev/null", "w", stderr);
    execvp(args[0], (char**)args);
  }
  else {
    int status;
    wait(&status);
    return !status;
  }
}

int FileReader::get_size(const path& p) {
  return file_size(p);
}

string FileReader::get_language(const string& filename) {
  string extension = filename.substr(filename.find_last_of('.') + 1);
  if(extension == "c")
    return "c";
  
  const int N = 9;
  string cpp[N] = {"h", "hh", "hpp", "hxx", "h++", "cc", "cpp", "cxx", "c++"};
  if(find(cpp, cpp + N, extension) != cpp + N)
    return "c++";
  
  return "";
}

void FileReader::read_file(const path& p, string& output) {
  ifstream ifs(p.c_str());
  output = string((istreambuf_iterator<char>(ifs)), istreambuf_iterator<char>());
}

