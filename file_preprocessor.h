#ifndef MASTERTHESIS_FILE_PREPROCESSOR_H
#define MASTERTHESIS_FILE_PREPROCESSOR_H

#include "preprocessed_file.h"
#include <map>
#include <string>

class FilePreprocessor {

 public:

  bool process(PreprocessedFile& file);

 private:

  std::string get_mapping(const std::string& token);

  bool is_basic_type(const std::string& token);
  
  static const std::map<std::string, std::string> mapping;
};

#endif
