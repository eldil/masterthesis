#include "constants.h"
#include <boost/algorithm/string.hpp>
using namespace std;
using namespace boost;

//Init:
void init(int argc, char* argv[]) {
  google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_admins = ":" + FLAGS_admins + ":";
  FLAGS_debug = ":" + FLAGS_debug + ":";
}

//Parsing flags:
const string DEBUG::ALL = "all";
const string DEBUG::SATORI = "satori";
const string DEBUG::FILE_DATA = "files";
const string DEBUG::COMPILATION = "compilation";

bool DEBUG::show(const vector<string> PARAMS) {
  if(FLAGS_debug.find(DEBUG::ALL) != string::npos)
    return true;
  for(auto param: PARAMS)
    if(FLAGS_debug.find(":" + param + ":") != string::npos)
      return true;
  return false;
}

vector<pair<string, string>> parse_problems() {
  vector<pair<string, string>> result;
  vector<string> contests, problems;
  split(contests, FLAGS_contest, is_any_of(","), token_compress_on);
  for(string& contest: contests)
    result.push_back(make_pair(contest, ""));
  split(problems, FLAGS_problem, is_any_of(","), token_compress_on);
  for(string& problem: problems) {
    if(problem.empty())
      continue;
    vector<string> parts;
    split(parts, problem, is_any_of(":"), token_compress_on);
    if(parts.size() != 2 || parts[0].empty() || parts[1].empty())
      throw logic_error("Invalid format of the problems flag, \
expected is CONTEST1:PROBLEM1;CONTEST2:PROBLEM2;...");
    result.push_back(make_pair(parts[0], parts[1])); 
  }
  return result;
}

//Mode:
DEFINE_bool(satori, false,
    "files downloaded from the Satori testing system");
DEFINE_bool(stdin, false,
    "filepaths provided on the stdin in separate lines, ending with an empty line");
DEFINE_string(athina, "",
    "path to the folder with athina data");

DEFINE_int32(maxlen, 10,
    "maximum length of sequences of tokens added/selected from database");
DEFINE_string(database, "database/OKhashed10.db",
    "path to the database used");
DEFINE_string(contest, "",
    "for athina: contest regexp; for satori: contest names separated with ,");
DEFINE_string(problem, "",
    "for athina: problem name (contest provided by contest flag; \
     for satori: contest1:problem1,contest2:problem2,...");
DEFINE_string(user, "",
    "username for searching satori/athina files");
DEFINE_string(submits, "data",
    "directory for storing satori submits");
DEFINE_string(cxxflags, "",
    "extra flags to be passed to clang++");
DEFINE_int32(start, 0,
    "the starting number of the file for training - building database");
DEFINE_int32(progress, 10,
    "the frequency, the information about training/testing is dispalyed with");
DEFINE_int32(rank, 100,
    "number of rows of the plagiarism ranking that are printed");
DEFINE_int64(maxscore, 1000,
    "maximum score for one match");
DEFINE_string(admins, ":admin:",
    "users which are admins (their submissions ignored by the system), \
    separated by ':'");
DEFINE_string(status_regex, "",
    "a regular expression for filtering submits by status");
DEFINE_string(debug, "",
    "print detailed information about programm execution, supported \
    parameter are defined in DEBUG struct, separate with ':'");

DEFINE_bool(extending, true,
    "extend match to increase score and display it during file comparison");
DEFINE_bool(mismatch, true,
    "extend match allowing one pair of mismatched tokens");
DEFINE_bool(onlyacc, true,
    "file_searcher finds only files that received OK");
DEFINE_bool(onlylast, false,
    "file_searcher finds only the last files uploaded by each user");
DEFINE_bool(cpp0x, false,
    "files should be parsed with -stc=c++0x option");

