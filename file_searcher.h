#ifndef MASTERTHESIS_FILE_SEARCHER_H
#define MASTERTHESIS_FILE_SEARCHER_H

#include "file_reader.h"
#include "file_data.h"
#include <vector>
#include <map>
#include <boost/filesystem.hpp>
using boost::filesystem::path;

class FileSearcher {

 public:
  
  FileSearcher();

  void set_backup(const path& dest_dir, int frequency);

  void get_files(std::vector<FileData>& output);

  void sort_files();

  bool get_file_text(int nr, std::string& output);

  //path to athina files folder; regEx for contest name; problem name
  void add_athina_files(
      const path& athina_path,
      const std::string contest_regex = ".*",
      const std::string problem_name = ""
      );

  void copy_files(int start, int end);

  void reset();

 private:

  void backup_data();

  int level;
  int frequency;
  int last_backup;
  path dest_dir;
  std::vector<FileData> files;
  std::map<std::string, int> users;
  FileReader reader;

};

#endif

