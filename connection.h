// vim:ts=4:sts=4:sw=4:expandtab
#ifndef SATORI_CONNECTION_H
#define SATORI_CONNECTION_H

#include <sys/socket.h>
#include <arpa/inet.h>
#include <string>
#include <thrift/protocol/TProtocol.h>
#include <thrift/transport/TSSLSocket.h>

namespace satori {
    class Connection {
        public:
            struct Configuration {
                std::string config;
                std::string section;
                std::string host;
                unsigned short int thrift_port;
                unsigned short int blob_port;
                std::string username;
                std::string contestant;
                std::string machine;
                std::string password;
                bool ssl;
                std::string certificate;
                void setup();
                Configuration();
            };
            Connection(const Configuration&);
        private:
            Configuration config;
            std::string token;
            boost::shared_ptr<apache::thrift::transport::TSSLSocketFactory> ssl_factory;
            boost::shared_ptr<apache::thrift::protocol::TProtocol> protocol;
            void connect_protocol();
        public:
            boost::shared_ptr<apache::thrift::protocol::TProtocol> connect_thrift();
            const std::string& get_token();
            bool download_blob(const std::string& path, const std::string& model, size_t id, const std::string& group, const std::string& name);
    };
}

#endif //SATORI_CONNECTION_H
